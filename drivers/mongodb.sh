#!/bin/bash
if [ -n "$MONGO_URI" ]; then
mongodump --uri "$MONGO_URI" --archive="$DESTINATION_FILE_PATH" --gzip
else
mongodump -h "$DB_HOST:$DB_PORT"  -d "$DB_DATABASE" -u "$DB_USERNAME" -p "$DB_PASSWORD" --authenticationDatabase="$MDAUTHDB" --archive="$DESTINATION_FILE_PATH" --gzip
fi