# Backup with Gitlab-Scheduled-Pipeline

This repositrory include script and pipline configuration to manage backuping multiple type of databases and files.

This Pipline Configuration consist of three stages:
+ **backup**: for backuping databases or datapath and stored in a compressed file.
+ **store**: push the compressed file to the s3 object storage of move to another path.
+ **prune**: remove old backup file in purpose of just remaining the specified number of files in storege. 

## How to use it
1. First of all deploy a [gitlab-runner](https://docs.gitlab.com/runner/) in your enviroment.
this enviroment must have needed backuping tools installed already.
if you'r using docker, you can use [these container images](https://gitlab.com/regular-backup/gitlab-runner) for make setup easier.

2. Make a repository on gitlab.com or any other instance of gitlab service and fill that with [this snippet](https://gitlab.com/regular-backup/scheduled-pipeline/-/snippets/2177558) or even fork or clone this repository.
3. Register that runner for the repo that you made in previous step.
4. [Configuring pipeline schedules](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#configuring-pipeline-schedules "Permalink") with appropriate environment variables that explained in the next part.

## Environment Variables
There is some kind of variables that you should choose those are appropriate for your scenario.
### Common variables
| Name | Description |
|--|--|
| BACKUP_TYPE | explained more in the following |
| REMAIN_BACKUP_NUMBER | number of backups that must remain in the storage. (if the number of backups reached this, the oldest backup will be removed in the *prune* stage) |

### Store in Object Storage
| Name | Description |
|--|--|
| S3_ADDR | Endpoint of object storage server |
| S3_ACCESS | Access Key for object storage |
| S3_SECRET | Secret Key for object storage |
| S3_BUCKET | Bucket Name |

### Store in Block Storage or Local Path
| Name | Description |
|--|--|
| LOCAL_STORAGE_PATH | Path of directory that all backup files should be stored |

### Backup from DataBases
Types of database that this script support:
- `mongodb`
- `mysql`
- `postgresql`

You should set one of the above name for the `BACKUP_TYPE` variable.

And config credentials with these variables:
| Name | Description |
|--|--|
| DB_HOST | Address of database server |
| DB_PORT | Port that database server listened to it |
| DB_DATABASE | Name of database that actual data stored in it |
| DB_USERNAME | Username that have access to data |
| DB_PASSWORD | Password of user for authentication |
| MDAUTHDB | *just for mongodb*. Authentication Database Name |

> For *mongodb* you can just set **`MONGO_URI`** instead of all the variables in the table.

### Backup from DataPath
Types of compression that this script support:
- `gz`
- `zip`

You should set one of the above name for the `BACKUP_TYPE` variable.

| Name | Description |
|--|--|
| DATA_PATH | Data path |
