#!/bin/bash

FILES=$(sed -n 's/^\[[^][]*][[:blank:]]*[^[:blank:]]*[[:blank:]]\(.*\)$/\1/p' <<< "$(mc ls $DESTINATION/$S3_BUCKET)")
echo "ALL: " $FILES

DELETABLE_COUNT=$(($(echo "$FILES" | wc -l) - $REMAIN_BACKUP_NUMBER))
echo "$DELETABLE_COUNT files must be removed"

DELETABLE_FILES=$(echo "$FILES" | head -n $DELETABLE_COUNT)
echo "Deletable Files: " $DELETABLE_FILES


echo "$DELETABLE_FILES" | xargs -I{} mc rm $DESTINATION/$S3_BUCKET/{}