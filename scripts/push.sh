#!/bin/bash
DATE_FORMAT=$(date +"%Y-%m-%d--%H-%M-%S")

if [ "$BACKUP_TYPE" == "zip" ]; then 
        FILENAME=$DATE_FORMAT.zip 
elif [ "$BACKUP_TYPE" == "mongodb" ]; then 
        FILENAME=$DATE_FORMAT.mongo.gz
elif [ "$BACKUP_TYPE" == "gz" ]; then 
        FILENAME=$DATE_FORMAT.gz 
else
        FILENAME=$DATE_FORMAT.sql.gz
fi

mc cp $DESTINATION_FILE_PATH $DESTINATION/$S3_BUCKET/$FILENAME
